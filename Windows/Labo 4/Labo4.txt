4.1) 

a) Heeft GPT partities die beter zijn tegen virussen & rootkits die zich in de opstartsector kunnen nestelen, waardoor het besturingssysteem niet meer wil opstarten.

b) Bios heeft de basisvoorzieningen, terwijl UEFI meer een besturingsysteem op zichzelf is.

c) Heeft een snellere opstartsnelheid omdat die met alle hardware compatibel is.

d) UEFI kan moeilijk oudere besturingssystemen van windows installeren.



4.2) 


3) De virtuele machine start op in Windows 7 omdat eerst de harde schijf van windows 7 wordt opgestart alvorens deze van windows 98 wordt opgestart.



4.3) 

1) Hier kom je in 2 opties terecht:

	optie 1: Windows-opstartbeheer

	optie 2: Windows-opstartlaadprogramma

2) 
	a) bcdedit/create /d "Linux" /application osloader

	b) C:\Windows\system32>bcdedit/set {5cc63c74-e82e-11e0-b09e-eaab89ebc234} device partition=C:

	c) C:\Windows\system32>bcdedit/displayorder {5cc63c74-e82e-11e0-b09e-eaab89ebc234} /addlast

3) bcdedit/timeout 32



4.4)

6) bootrec.exe 

	a) /FixMbr: Deze optie wordt een Windows 7 of Windows Vista-compatibele MBR weggeschreven naar de systeempartitie. De bestaande partitietabel overschreven niet. Gebruik deze optie wanneer u MBR-problemen moet oplossen of wanneer u niet-standaardcode uit de MBR moet verwijderen.

	b) /FixBoot: Deze optie wordt een nieuwe opstartsector geschreven naar de systeempartitie met behulp van een opstartsector die compatibel is met Windows Vista of Windows 7. 

	c) /ScanOs: Met deze optie worden alle schijven voor installaties die compatibel met Windows Vista of Windows 7 zijn. Het bevat ook de posten die momenteel niet in het BCD-archief.

	d) /RebuildBcd: Met deze optie worden alle schijven voor installaties die compatibel met Windows Vista of Windows 7 zijn. Daarnaast kunt u aangeven welke installaties u wilt toevoegen aan het BCD-archief.

7) bcdedit/delete


4.5) 

1) systeem --> geavanceerde instellingen --> opstart-en herstelinstellingen --> timeout aanpassen

2) . %SystemRoot%\MEMORY.DMP