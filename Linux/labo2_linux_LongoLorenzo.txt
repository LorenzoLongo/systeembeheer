Oefening 1

3) Date - Geeft de huidige tijd en datum weer.

   pwd - Print Working Directory. Geeft de output weer van de huidige directory.

   ls - Geeft de inhoud van de huidige directory weer.

   clear - wist alle commands die al reeds op de terminal zijn uitgevoerd.

4) ls -a:  Geeft de inhoud van de huidige directory weer, met alle bestanden, waarvan ook de verborgen "." bestanden.

    ls -al: Geeft een meer gedetailleerde directory weer, met de inhoud van die directory, en zijn verborgen "." bestanden.

    ls -R:  Geeft de inhoud van de huidige directory weer, en gaat ook alle subdirectory's af.

    cat .bashrc: met cat bekijk je de inhoud van het .bashrc bestand weer in de terminal.

    mkdir werk: Maakt een nieuwe directory aan die de naam "werk" heeft.

    cd werk: (CD = Change Directory) met deze command verander je dus van directory (in dit geval de directory "werk").

    cd ..: Gaat naar de onmiddelijke hogere directory in de directorytree. 

    rm -r werk: is het recursief verwijderen van de directory die de naam "werk" bevat. (rm = remove).

    cp .bashrc test.txt: (cp = copy) kopieert de inhoud van het .bashrc bestand naar een nieuw text bestand dat "test" noemt.

     mv test.txt test1.txt: (mv = move) Hiermij verander je de naam van het oorspronkelijke bestand test.txt naar test1.txt.

     wc test1.txt: (wc = word count) Telt het aantal woorden, regels en tekens van het bestand test1.txt op.

     wc -l test1.txt: Telt enkel het aantal regels van dat bestand op.

    rm test1.txt:  Verwijdert het test1.txt bestand. 


Oefening 2

1) cp .bashrc testfile; cat testfile: Kopieert de .dashrc file naar een nieuwe file, onder de naam van 'testfile', en opent vervolgens de gekopieerde "testfile" met het 'cat testfile' commando.

   ls -l testfile; rm testfile; ls -l testfile: Geeft de directory weer waar de "testfile" zich in bevindt, door de optie -l wordt de directory meer gedetailleerd weergegeven. Met het daaropvolfende commando (rm testfile) verwijder je het bestand "testfile", en tenslotte vraag je nog eens met het commando ls -l testfile om de directory van het testfile bestand weer te geven, maar omdat deze reeds gewist is door het vorige commando, kan deze niet meer worden opgevraagd.

2) Met het commando (Cal "Maand" "Jaar") vb. cal februari 1993

3) cd /etc

4) ls -a *

5) ls -a ????


Oefening 3

1) man ls  --> 1) -a: Geeft alle bestanden weer, ook de verborgen ”.”bestanden;
				  --no-group:^print de groepsnamen niet uit in een lange lijst.
				  -i: print het indexnummer van elk bestand.
				  -l: gebruik maken van het lange lijst formaat.

   man cp  --> 2) --attributes-only: Kopieert de data van het bestand niet, enkel de attributen ervan.
   				  --backup: Maakt een backup van elk bestaand bestand (file).
   				  -l: linked de bestanden in plaats van ze te kopieren.
   				  --version: geeft informatie over de versie die gebruikt word.

   man rm  --> 3) 


   man man --> 4) -I, --match-case: Hoofdletters zijn van invloed bij het zoeken naar man-pagina's.
   			      -i, --ignore-case: Hoofdletters   zijn   niet  van  invloed  bij  het  zoeken  naar man-pagina's.  Dit is het standaardgedrag.
   				  -c, --catman: De optie is niet voor algemeen gebruik en  mag  alleen  gebruikt worden door het programma catman.
   				  -f, --whatis: Equivalent  aan  whatis. Toon  een  korte  beschrijving van deman-pagina, mits deze beschikbaar is. Zie whatis(1)   voor  meer informatie.




2) whoami:  Geeft het gebruikte userid weer.
   who: Geeft weer welke gebruikers er op het systeel zijn aangemeld.



